class BillingsController < ApplicationController
  before_filter :billing_check, only: [:show]

  def new
    @billing = Billing.new
  end

  def create
    @billing = Billing.create(billing_params)
    if @billing.save
      redirect_to billing_path(current_user)
    end
  end

  def show
    @billing = Billing.where(user_id: current_user.id).first
    @cart_items = CartItem.where(cart_id: current_user.cart.id, is_active: true)

  end

  private

  def billing_params
    params.require(:billing).permit(:user_id, :first_name, :last_name, :contact_no, :address1, :address2, :city, :state, :country, :pincode, :is_primary)
  end

  def billing_check
    unless current_user.billings.present?
      redirect_to new_billing_path
    end
  end
end
