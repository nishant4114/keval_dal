class CartsController < ApplicationController
  before_action :authenticate_user!
  before_filter :cart_total, only: [:place_order]

  def show
 @cart_items = CartItem.where(cart_id: current_user.cart.id, is_active: true)

  end

  def add_to_cart
    params[:cart_item][:cart_id] = current_user.cart.id
    cart_item_present = CartItem.where(cart_id: current_user.cart.id, variant_id: params[:cart_item][:variant_id], is_active: true)
    quantity  = params[:cart_item][:quantity].to_i

    if quantity != 0 && (quantity.is_a? Integer)
      p 'integer'


      if cart_item_present.present?
        flash[:error] = 'The item is already present in your cart'
        respond_to do |format|
          format.js {render js: "window.location = '#{product_path(params[:id])}';"}
        end
        return false
      end
      @cart_item = CartItem.create(cart_params)
      if @cart_item.save
        respond_to do |format|
        format.js {render js: "window.location = '#{cart_path(current_user.cart)}';"}
        end
      end
    else
      respond_to do |format|
        format.js
      end
    end
  end

  def update_cart
    @cart_items = CartItem.where(cart_id: params[:id])
    update_cart_params.keys.each do |cart_item_id|
      cart_item = @cart_items.find(cart_item_id)
      if cart_item
        cart_item.update(update_cart_params[cart_item_id])
      end
    end
    redirect_to cart_path(current_user.cart)
  end

  def delete_cart_item
   @cart_item = CartItem.find(params[:id])
    @cart_item.destroy
    redirect_to cart_path(current_user.cart)
  end

  def place_order
    p params

    @cart = Cart.where(user_id: current_user.id).first
    @cart_items = CartItem.where(cart_id: @cart.id, is_active: true)
    p  @cart_total
    CartMailer.place_order_admin_mail(@cart_items, current_user, @cart_total, current_user.billings[0], params[:order][:schedule_date], params[:order][:schedule_time]).deliver
    @previous_cart = PreviousCart.new(:user_id => current_user.id, :total => @cart_total)
    if @previous_cart.save
      @order = Order.new(order_params)
      @order.save
      @cart_items = CartItem.where(cart_id: current_user.cart.id)
      @cart_items.each do |cart_item|
        cart_item.update_attributes(:is_active => false, :previous_cart_id => @previous_cart.id)
      end
      CartMailer.place_order_mail(current_user).deliver
      flash[:notice] = 'Thank You for Placing the order. Our team will call you in a while'
      redirect_to order_completed_path
    end
  end

  private

  def cart_params
    params.require(:cart_item).permit(:quantity, :cart_id, :variant_id)
  end

  def update_cart_params
    params.require(:cart_items).permit!
  end
  def order_params
    params.require(:order).permit(:schedule_date, :schedule_time, :user_id)
  end

  def cart_total
    @cart_items = CartItem.where(cart_id: current_user.cart.id, is_active: true)
    p 'sdsddsdssdsdsdds'
    p @cart_items
    @cart_total = 0
    @cart_items.each do |cart_item|
        p 'inside'
        @cart_total += (cart_item.variant.price * cart_item.quantity)
    end
   p @cart_total
  end
end