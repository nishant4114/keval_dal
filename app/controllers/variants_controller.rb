class VariantsController < ApplicationController
  def new
    @variant = Variant.new
    @variant.variant_photos.build
  end

  def edit
    @variant = Variant.where(id: params[:id]).first

  end

  def update
    @variant = Variant.where(id: params[:id]).first

    if @variant.update_attributes(variants_params)
      redirect_to products_path
    end
  end

  def create
    @variant = Variant.create(variants_params)
    if @variant.save
      flash[:success] = "variant saved"
      redirect_to products_path
    else
      flash[:error] = "Error"
      render :new
    end
  end

  def show

  end

  private

  def variants_params
    params.require(:variant).permit(:name, :product_id, :price, :discount, :description, :variant_photo,  variant_photos_attributes:[:variant_image, :id, :variant_id])
  end

end
