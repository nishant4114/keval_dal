class RegistrationsController < Devise::RegistrationsController
  protected

  def after_sign_up_path_for(resource)

    @cart = Cart.new(:user_id => current_user.id)
    UserMailer.welcome_mail(current_user).deliver
    if @cart.save
      session[:previous_url] || root_path
    end

  end


end