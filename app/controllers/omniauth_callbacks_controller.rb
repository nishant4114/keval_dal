class OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def all
    user =  User.from_omniauth(request.env["omniauth.auth"])
    if user.persisted?
      sign_in_and_redirect user, notice: "Signed in"
     if user_signed_in? && current_user.cart.blank?
      @cart = Cart.new(:user_id => current_user.id)
      @cart.save
      UserMailer.welcome_mail(current_user).deliver
    end
    else
      if user_signed_in? && current_user.cart.blank?
        @cart = Cart.new(:user_id => current_user.id)
        UserMailer.welcome_mail(current_user).deliver
      end
      session["devise.user_attributes"] = user.attributes
      redirect_to products_path
    end
  end
  alias_method :facebook, :all
end
