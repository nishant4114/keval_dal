class ProductsController < ApplicationController
  def new
   @product = Product.new
    @categories = Category.all
  end

    def edit
      @product = Product.where(id: params[:id]).first
      @categories = Category.all
      p @product
    end

    def update
      @product = Product.where(id: params[:id]).first

      if @product.update_attributes(products_params)
        redirect_to products_path
      end
    end

  def create
    @product = Product.create(products_params)
    if @product.save
      flash[:success] = "Product saved"
      redirect_to products_path
    else
      flash[:error] = "Error"
      render :new
    end
  end

  def index
    @products = Product.all

  end

  def show
    @product = Product.where(id: params[:id]).first
    @product_variants = Variant.where(product_id: params[:id])

  end

  private

  def products_params
    params.require(:product).permit(:name, :description, :category_id, :product_image)
  end
end
