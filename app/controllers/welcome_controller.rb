class WelcomeController < ApplicationController

  def index
    @product_variants = Variant.all.limit(6)

    p @product_variants
  end

  def about

  end

  def contact

  end

  def order_completed

  end
end
