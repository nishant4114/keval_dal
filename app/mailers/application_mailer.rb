class ApplicationMailer < ActionMailer::Base
  default from: "nishant.varshney4114@gmail.com"
  layout 'mailer'
end
