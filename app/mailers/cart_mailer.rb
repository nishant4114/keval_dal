class CartMailer < ApplicationMailer
  def place_order_mail(user)
    mail(to: user.email, subject: 'Order Placed')
    @user = user
  end

  def place_order_admin_mail(cart_items, user, total, billing, schedule_date, schedule_time)
    p cart_items
    @cart_items = cart_items
    @user = user
    @total = total
    @billing = billing
    @schedule_date = schedule_date
    @schedule_time = schedule_time
    mail(to: 'kevaldal.inc@gmail.com', subject: 'Order Placed')
  end
end