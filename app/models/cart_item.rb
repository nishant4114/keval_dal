class CartItem < ActiveRecord::Base
  belongs_to :cart
  belongs_to :variant
  belongs_to :previous_cart
end
