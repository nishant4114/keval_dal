class Product < ActiveRecord::Base
  has_many :variants
  belongs_to :category

  has_attached_file :product_image, :styles => { medium: "300x300>", thumb: "100x100>" },
                    :s3_protocol => :https,
                    :url => ':s3_domain_url',
                    :path => '/product_image/:id/:style_:basename.:extension'

  validates_attachment_content_type :product_image, content_type: /\Aimage\/.*\Z/
end
