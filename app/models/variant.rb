class Variant < ActiveRecord::Base
  belongs_to :product
  has_many :variant_properties
  has_many :variant_photos
  accepts_nested_attributes_for :variant_photos, :allow_destroy => true
end
