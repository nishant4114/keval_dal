class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :omniauthable,
         :recoverable, :rememberable, :trackable, :validatable
  has_one :cart
  has_many :billings
  has_many :previous_carts
  has_many :orders

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.email = auth["extra"]["raw_info"]["email"]
      user.provider = auth.provider
      user.uid = auth.uid

    end
  end

  def self.new_with_session(params, session)
    if session["devise.user_attributes"]
      new(session["devise.user_attributes"], without_protection: true) do |user|
        user.attributes = params
        user.valid?
      end

    else
      super
    end
    # super.tap do |individual|
    #   if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
    #     individual.email = data["email"] if individual.email.blank?
    #   end
    # end
  end

  def password_required?
    super && provider.blank?
  end
  def email_required?
    super && provider.blank?
  end

end
