class VariantPhoto < ActiveRecord::Base
  belongs_to :variant


  has_attached_file :variant_image, :styles => { medium: "300x300>", thumb: "100x100>" },
                    :s3_protocol => :https,
                    :url => ':s3_domain_url',
                    :path => '/variant_image/:id/:style_:basename.:extension'
  validates_attachment_content_type :variant_image, content_type: /\Aimage\/.*\Z/
end
