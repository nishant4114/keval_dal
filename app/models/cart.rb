class Cart < ActiveRecord::Base
  belongs_to :user
  has_many :variants
  has_many :cart_items
  accepts_nested_attributes_for :cart_items

  scope :active_cart, -> { where(order_completed: false) }
end
