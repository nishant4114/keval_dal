module CartsHelper
  def variant_price(variant_id)
    @variant = Variant.where(id: variant_id).first
    return @variant.price
  end
end
