module ApplicationHelper
  def cart_total
    @cart_items = CartItem.where(cart_id: current_user.cart.id, is_active: true)
    @cart_total = 0
    @cart_items.each do |cart_item|
        @cart_total += (cart_item.variant.price * cart_item.quantity)
    end
    return @cart_total
  end

  def total_active_cart_items
    @cart_items = CartItem.where(cart_id: current_user.cart.id, is_active: true)
    @cart_item_total = @cart_items.count
    return @cart_item_total
  end

end
