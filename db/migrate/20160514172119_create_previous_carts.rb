class CreatePreviousCarts < ActiveRecord::Migration
  def change
    create_table :previous_carts do |t|

      t.belongs_to :user
      t.float :total
      t.string :mode_payment
      t.timestamps null: false
    end
  end
end
