class ChangeDateTime < ActiveRecord::Migration
  def change
    rename_column :orders, :schedule, :schedule_date
    add_column :orders, :schedule_time, :string
  end
end
