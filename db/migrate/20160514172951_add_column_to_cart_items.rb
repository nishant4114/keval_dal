class AddColumnToCartItems < ActiveRecord::Migration
  def change
   add_column :cart_items, :is_active, :boolean, :default => true
   add_column :cart_items, :previous_cart_id, :integer
  end
end
