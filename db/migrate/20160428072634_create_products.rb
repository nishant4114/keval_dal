class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.belongs_to :category, index: true
      t.integer :price
      t.boolean :product_available, :default => false
      t.text :description
      t.float :quantity_sold

      t.timestamps null: false
    end
  end
end
