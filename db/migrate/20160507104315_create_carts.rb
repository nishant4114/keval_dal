class CreateCarts < ActiveRecord::Migration
  def change
    create_table :carts do |t|
      t.belongs_to :user
      t.float :total
      t.boolean :order_completed, :default => false
      t.timestamps null: false
    end
  end
end
