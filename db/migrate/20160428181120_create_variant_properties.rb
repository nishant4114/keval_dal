class CreateVariantProperties < ActiveRecord::Migration
  def change
    create_table :variant_properties do |t|
      t.belongs_to :variant, :index => true
      t.string :name
      t.string :value
      t.timestamps null: false
    end
  end
end
