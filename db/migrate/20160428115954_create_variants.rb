class CreateVariants < ActiveRecord::Migration
  def change
    create_table :variants do |t|
      t.string :name
      t.belongs_to :product, :index => true
      t.integer :price
      t.integer :discount
      t.boolean :availability
      t.text :description
      t.timestamps null: false
    end
  end
end
