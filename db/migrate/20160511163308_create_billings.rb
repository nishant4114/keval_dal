class CreateBillings < ActiveRecord::Migration
  def change
    create_table :billings do |t|
      t.belongs_to :user
      t.string :first_name
      t.string :last_name
      t.string :contact_no
      t.string :address1
      t.string :address2
      t.string :city
      t.string :state
      t.string :country
      t.string :pincode
      t.boolean :is_primary

      t.timestamps null: false
    end
  end
end
