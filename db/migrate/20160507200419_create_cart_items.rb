class CreateCartItems < ActiveRecord::Migration
  def change
    create_table :cart_items do |t|
      t.belongs_to :variant
      t.belongs_to :cart
      t.float :quantity
      t.timestamps null: false
    end
  end
end
