class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.belongs_to :user
      t.string :schedule
      t.boolean :is_delivered,  :default => false
      t.timestamps null: false
    end
  end
end
