class CreateVariantPhotos < ActiveRecord::Migration
  def change
    create_table :variant_photos do |t|
      t.belongs_to :variant
      t.attachment :variant_image
      t.timestamps null: false
    end
  end
end
